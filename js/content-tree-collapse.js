(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.contentTreeCollapse = {
    attach: function (context) {
      var menu_links = $(once('content-tree-collapse', '#content-tree-table', context)).find('.menu-link');
      menu_links.filter('.menu-parent').on('click', '.collapse-button',function (event) {
        event.preventDefault();
        contentTreeCollapse($(event.delegateTarget));
      });

      function contentTreeCollapse(parent) {
        var id = parent.attr('id');
        var hide = !parent.hasClass('tree-collapsed');
        hide ? parent.addClass('tree-collapsed') : parent.removeClass('tree-collapsed');
        menu_links.each(function (index, element) {
          var menu_link = $(element);
          if (menu_link.attr('tree-parent') === id) {
            hide ? menu_link.addClass('hidden') : menu_link.removeClass('hidden');
            if (hide && menu_link.hasClass('menu-parent') && !menu_link.hasClass('tree-collapsed')) {
              contentTreeCollapse(menu_link);
            }
          }
        });
      }
    }
  };
})(jQuery, Drupal);
