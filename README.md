#Content hub tree

INTRODUCTION
------------

The Content hub tree provides form that looks like the content view,
but in a menu-like structure.


CONFIGURATION
-------------

 * To enable content thee hub for some menu
   (/admin/structure/menu/manage/{menu})
   just check the checkbox "Include in Content tree".
