<?php

namespace Drupal\content_hub_tree\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Table;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\content_hub_tree\HelperService;
use Drupal\menu_link_content\MenuLinkContentStorageInterface;
use Drupal\system\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Base form for menu edit forms.
 *
 * @internal
 */
class MenuContentTreeForm extends EntityForm {

  /**
   * Hide menus that on this level and deeper.
   */
  const HIDE_MENUS_LVL = 2;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * The menu tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * The link generator.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * The menu_link_content storage handler.
   *
   * @var \Drupal\menu_link_content\MenuLinkContentStorageInterface
   */
  protected $menuLinkContentStorage;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The overview tree form.
   *
   * @var array
   */
  protected $overviewTreeForm = ['#tree' => TRUE];

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Nodes.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  private $nodes;

  /**
   * Links mapping to node.
   *
   * @var array
   */
  private $linksMapping;

  /**
   * Content tree helper.
   *
   * @var \Drupal\content_hub_tree\HelperService
   */
  private $helper;

  /**
   * Constructs a MenuForm object.
   *
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link manager.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator.
   * @param \Drupal\menu_link_content\MenuLinkContentStorageInterface $menu_link_content_storage
   *   The menu link content storage handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\content_hub_tree\HelperService $helper_service
   *   The service object.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration object.
   */
  public function __construct(MenuLinkManagerInterface $menu_link_manager, MenuLinkTreeInterface $menu_tree, LinkGeneratorInterface $link_generator, MenuLinkContentStorageInterface $menu_link_content_storage, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, HelperService $helper_service, ConfigFactory $configFactory) {
    $this->menuLinkManager = $menu_link_manager;
    $this->menuTree = $menu_tree;
    $this->linkGenerator = $link_generator;
    $this->menuLinkContentStorage = $menu_link_content_storage;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->helper = $helper_service;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.menu.link'),
      $container->get('menu.link_tree'),
      $container->get('link_generator'),
      $container->get('entity_type.manager')->getStorage('menu_link_content'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('content_hub_tree.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * Provide tabs to configure menu.
   */
  public function selectMenu(&$form) {
    $menus = $this->entityTypeManager->getStorage('menu')->loadByProperties(['content_hub_tree' => TRUE]);
    $form['select_menu'] = [
      '#weight' => -100,
      '#type' => 'select',
      '#title' => $this->t('Select menu'),
      '#options' => array_map(function (Menu $menu) {
        return $menu->label();
      }, $menus),
      '#default_value' => $this->entity->id(),
      '#ajax' => [
        'callback' => '::selectMenuCallback',
        'event' => 'change',
        'progress' => [
          'type' => 'fullscreen',
        ],
      ],
    ];
  }

  /**
   * Select menu.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function selectMenuCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $form_state->getValue('select_menu');
    $response->addCommand(new RedirectCommand(Url::fromRoute(
      'content_hub_tree.content_hub_tree',
      ['menu' => $form_state->getValue('select_menu')]
    )->toString()));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $this->selectMenu($form);

    $menu = $this->entity;

    $form['#title'] = $this->t('Content tree: %label', ['%label' => $menu->label()]);

    // Add menu links administration form for existing menus.
    if (!$menu->isNew() || $menu->isLocked()) {
      // Form API supports constructing and validating self-contained sections
      // within forms, but does not allow handling the form section's submission
      // equally separated yet. Therefore, we use a $form_state key to point to
      // the parents of the form section.
      // @see self::submitOverviewForm()
      $form_state->set('menu_overview_form_parents', ['links']);
      $form['links'] = [];
      $form['links'] = $this->buildOverviewForm($form['links'], $form_state);
    }

    $this->getBulkOperations($form, $form_state);

    return parent::form($form, $form_state);
  }

  /**
   * Try to find node to which linked this menu link.
   */
  private function findNode($plugin_id) {
    if ($this->nodes === NULL) {
      $menu_links = $this->menuLinkContentStorage->loadByProperties(['menu_name' => $this->entity->id()]);
      $this->linksMapping = [];
      foreach ($menu_links as $link) {
        $url = $link->getUrlObject();
        if ($url->isExternal() || !$url->isRouted()) {
          continue;
        }
        if ($url->getRouteName() === '<front>') {
          $url = Url::fromUserInput($this->configFactory->get('system.site')
            ->get('page.front'));
        }
        if ($url->getRouteName() === 'entity.node.canonical') {
          $this->linksMapping[$link->getPluginId()] = (int) $url->getRouteParameters()['node'];
        }
      }
      $this->nodes = $this->entityRepository->getActiveMultiple('node', $this->linksMapping);
    }
    return $this->nodes[$this->linksMapping[$plugin_id] ?? NULL] ?? NULL;
  }

  /**
   * Form constructor to edit an entire menu tree at once.
   *
   * Shows for one menu the menu links accessible to the current user and
   * relevant operations.
   *
   * This form constructor can be integrated as a section into another form. It
   * relies on the following keys in $form_state:
   * - menu: A menu entity.
   * - menu_overview_form_parents: An array containing the parent keys to this
   *   form.
   * Forms integrating this section should call menu_overview_form_submit() from
   * their form submit handler.
   */
  protected function buildOverviewForm(array &$form, FormStateInterface $form_state) {
    // Ensure that menu_overview_form_submit() knows the parents of this form
    // section.
    if (!$form_state->has('menu_overview_form_parents')) {
      $form_state->set('menu_overview_form_parents', []);
    }

    $form['#attached']['library'][] = 'menu_ui/drupal.menu_ui.adminforms';

    $tree = $this->menuTree->load($this->entity->id(), new MenuTreeParameters());

    // We indicate that a menu administrator is running the menu access check.
    $this->getRequest()->attributes->set('_menu_admin', TRUE);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    $this->getRequest()->attributes->set('_menu_admin', FALSE);

    // Determine the delta; the number of weights to be made available.
    $count = function (array $tree) {
      $sum = function ($carry, MenuLinkTreeElement $item) {
        return $carry + $item->count();
      };
      return array_reduce($tree, $sum);
    };
    $delta = max($count($tree), 50);

    $form['links'] = [
      '#process' => [
        [Table::class, 'processTable'],
        '::processTable',
      ],
      '#type' => 'table',
      '#tableselect' => TRUE,
      '#theme' => 'table__menu_content_hub_tree',
      '#header' => [
        [
          'data' => $this->t('Title'),
          'class' => ['title-cell'],
        ],
        $this->t('Content type'),
        $this->t('Status'),
        $this->t('Author'),
        $this->t('Updated'),
        $this->t('Operations'),
      ],
      '#attributes' => [
        'id' => 'content-tree-table',
        'class' => [
          'content-tree-table',
        ],
      ],
    ];

    $form['links']['#empty'] = $this->t('There are no menu links yet. <a href=":url">Add link</a>.', [
      ':url' => Url::fromRoute('entity.menu.add_link_form', ['menu' => $this->entity->id()], [
        'query' => ['destination' => $this->entity->toUrl('edit-form')->toString()],
      ])->toString(),
    ]);

    $links = $this->buildOverviewTreeForm($tree, $delta);

    foreach (Element::children($links) as $id) {
      if (isset($links[$id]['#item'])) {
        $element = $links[$id];

        $form['links'][$id]['#item'] = $element['#item'];
        if (isset($element['#node'])) {
          $form['links'][$id]['#node'] = $element['#node'];
        }

        // TableDrag: Mark the table row as draggable.
        $form['links'][$id]['#attributes'] = $element['#attributes'];
        $form['links'][$id]['#attributes']['class'][] = 'draggable';

        // TableDrag: Sort the table row according
        // to its existing/configured weight.
        $form['links'][$id]['#weight'] = $element['#item']->link->getWeight();

        $element['id']['#attributes']['class'] = ['menu-id'];

        $form['links'][$id]['title'] = $element['title'];
        $form['links'][$id]['type'] = $element['type'] ?? [];
        $form['links'][$id]['status'] = $element['status'] ?? [];
        $form['links'][$id]['uid'] = $element['uid'] ?? [];
        $form['links'][$id]['changed'] = $element['changed'] ?? [];
        // Operations (dropbutton) column.
        $form['links'][$id]['operations'] = $element['operations'];
      }
    }

    return $form;
  }

  /**
   * Preprocess callback for table.
   */
  public function processTable($element, FormStateInterface $form_state, $form) {
    $element['#attached']['library'][] = 'content_hub_tree/content_hub_tree_collapse';
    foreach (Element::children($element) as $child) {
      // Disable select options for menu links.
      if (isset($element[$child]['select']) && !$this->findNode($child)) {
        $element[$child]['select']['#disabled'] = TRUE;
      }

      // Add ability to collapse elements.
      $row = &$element[$child];
      $row['#attributes']['class'][] = 'menu-link';
      if (!empty($row['#item']->subtree)) {
        $row['#attributes']['class'][] = 'menu-parent';
        $row['title'] = [
          [
            '#theme' => 'indentation',
            '#size' => $row['#item']->depth - 1,
          ],
          [
            '#type' => 'html_tag',
            '#tag' => 'button',
            '#attributes' => [
              'class' => [
                'collapse-button',
              ],
              'aria-label' => $this->t('Collapse/Expand button'),
            ],
            '#weight' => -1,
          ],
          $row['title'],
        ];
        $row['#item']->depth >= ($this::HIDE_MENUS_LVL - 1) && $row['#attributes']['class'][] = 'tree-collapsed';
      }
      else {
        $row['title'] = [
          [
            '#theme' => 'indentation',
            '#size' => $row['#item']->depth,
          ],
          $row['title'],
        ];
      }
      $row['title']['#wrapper_attributes']['class'] = [
        'content-tree-table-field--title',
        'content-tree-table-field',
      ];
      $row['#item']->depth >= $this::HIDE_MENUS_LVL && $row['#attributes']['class'][] = 'hidden';
      $row['#attributes']['data-level'] = $row['#item']->depth;

    }
    return $element;
  }

  /**
   * Recursive helper function for buildOverviewForm().
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The tree retrieved by \Drupal\Core\Menu\MenuLinkTreeInterface::load().
   * @param int $delta
   *   The default number of menu items used in the menu weight selector is 50.
   *
   * @return array
   *   The overview tree form.
   */
  protected function buildOverviewTreeForm(array $tree, $delta) {
    $form = &$this->overviewTreeForm;
    $tree_access_cacheability = new CacheableMetadata();
    foreach ($tree as $element) {
      $tree_access_cacheability = $tree_access_cacheability->merge(CacheableMetadata::createFromObject($element->access));

      // Only render accessible links.
      if (!$element->access->isAllowed()) {
        continue;
      }

      /** @var \Drupal\Core\Menu\MenuLinkInterface $link */
      $link = $element->link;
      if ($link) {
        $id = $link->getPluginId();

        $form[$id]['#item'] = $element;
        if (!$link->isEnabled()) {
          $form[$id]['title']['#suffix'] = ' (' . $this->t('disabled') . ')';
        }
        $form[$id]['#attributes'] = $link->isEnabled() ? ['class' => ['menu-enabled']] : ['class' => ['menu-disabled']];
        $node = $this->findNode($link->getPluginId()) ?? FALSE;
        if (!$node) {
          $form[$id]['title'] = Link::fromTextAndUrl($link->getTitle(), $link->getUrlObject())
            ->toRenderable();
          $form[$id]['operations'] = [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $this->t('Menu link'),
            '#attributes' => ['title' => $this->t('Menu link can be edited by visiting Menus in admin panel')],
          ];
        }
        else {
          $form[$id]['#node'] = $node;
          $this->renderNodeFields($form[$id], $node);
          $form[$id]['operations'] = [
            '#type' => 'operations',
            '#links' => $this->entityTypeManager->getListBuilder('node')->getOperations($node),
          ];
        }
        $form[$id]['#attributes']['id'] = $id;
        if ($link->getParent()) {
          $form[$id]['#attributes']['tree-parent'] = $link->getParent();
        }
      }

      if ($element->subtree) {
        $this->buildOverviewTreeForm($element->subtree, $delta);
      }
    }

    $tree_access_cacheability
      ->merge(CacheableMetadata::createFromRenderArray($form))
      ->applyTo($form);

    return $form;
  }

  /**
   * Render node fields.
   */
  private function renderNodeFields(array &$row, $node) {
    $fields = [
      'title' => 'title',
      'type' => 'type',
      'status' => 'status',
      'uid' => 'name',
      'changed' => 'changed',
    ];

    foreach ($fields as $key => $field) {
      $row[$key] = $this->helper->viewField($node, $key, $field);
      $row[$key]['#wrapper_attributes']['class'] = [
        "content-tree-table-field--$key",
        'content-tree-table-field',
      ];
    }
  }

  /**
   * Returns an array of supported actions for the current entity form.
   */
  protected function getBulkOperations(array &$form, FormStateInterface $form_state) {
    if (empty(Element::children($form['links']))) {
      return;
    }
    // Ensure a consistent container for filters/operations in the view header.
    $form['header'] = [
      '#type' => 'container',
      '#weight' => -99,
    ];

    $form['header']['bulk_operations'] = [
      '#type' => 'container',
    ];
    $form['header']['bulk_operations']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $this->helper->getBulkOptions(),
    ];

    $form['header']['bulk_operations']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected items'),
      '#submit' => ['::submitForm', '::applyBulkOperations'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = [];
    if (!empty($form['header']['bulk_operations']['submit'])) {
      $actions['bulk_operations'] = $form['header']['bulk_operations']['submit'];
    }

    return $actions;
  }

  /**
   * Execute bulk operation.
   */
  public function applyBulkOperations(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $selected = array_filter($user_input['links']);
    $entities = [];
    $action = $this->helper->getActions()[$form_state->getValue('action')];
    $count = 0;

    foreach ($selected as $bulk_form_key) {
      $entity = $this->findNode($bulk_form_key);
      // Skip execution if current entity does not exist.
      if (empty($entity)) {
        continue;
      }
      // Skip execution if the user did not have access.
      if (!$action->getPlugin()->access($entity, $this->currentUser())) {
        $this->messenger()->addError($this->t('No access to execute %action on the @entity_type_label %entity_label.', [
          '%action' => $action->label(),
          '@entity_type_label' => $entity->getEntityType()->getLabel(),
          '%entity_label' => $entity->label(),
        ]));
        continue;
      }

      $count++;

      $entities[$entity->id()] = $entity;
    }

    // If there were entities selected but the action isn't allowed on any of
    // them, we don't need to do anything further.
    if (!$count) {
      return;
    }

    $action->execute($entities);

    $operation_definition = $action->getPluginDefinition();
    if (!empty($operation_definition['confirm_form_route_name'])) {
      $options = [
        'query' => $this->getDestinationArray(),
      ];
      $form_state->setRedirect($operation_definition['confirm_form_route_name'], [], $options);
    }
    else {
      // Don't display the message unless there are some elements affected and
      // there is no confirmation form.
      $this->messenger()->addStatus($this->formatPlural($count, '%action was applied to @count item.', '%action was applied to @count items.', [
        '%action' => $action->label(),
      ]));
    }
  }

}
