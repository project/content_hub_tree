<?php

namespace Drupal\content_hub_tree;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\views\ViewExecutable;

/**
 * Class Helper sService.
 */
class HelperService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  private $viewBuilder;

  /**
   * Content view.
   *
   * @var \Drupal\views\ViewExecutable|null|false
   */
  private $contentView = NULL;

  /**
   * Bulk action storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $actionStorage;

  /**
   * Bulk actions.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  private $actions;

  /**
   * HelperService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $this->actionStorage = $entity_type_manager->getStorage('action');
    $this->actions = array_filter($this->actionStorage->loadMultiple(), function ($action) {
      return $action->getType() == 'node';
    });
  }

  /**
   * Returns the available operations for this form.
   *
   * @param bool $filtered
   *   (optional) Whether to filter actions to selected actions.
   *
   * @return array
   *   An associative array of operations, suitable for a select element.
   */
  public function getBulkOptions($filtered = TRUE) {
    $options = [];
    // Filter the action list.
    foreach ($this->actions as $id => $action) {
      if ($filtered && $this->contentView &&
        ($bulk_settings = $this->contentView->getHandler('default', 'field', 'node_bulk_form')) &&
        isset($bulk_settings['selected_actions'])
      ) {
        $in_selected = in_array($id, $bulk_settings['selected_actions']);
        // If the field is configured to include only the selected actions,
        // skip actions that were not selected.
        if (($bulk_settings['include_exclude'] == 'include') && !$in_selected) {
          continue;
        }
        // Otherwise, if the field is configured to exclude the selected
        // actions, skip actions that were selected.
        elseif (($bulk_settings['include_exclude'] == 'exclude') && $in_selected) {
          continue;
        }
      }

      $options[$id] = $action->label();
    }

    return $options;
  }

  /**
   * Render field with setting from content view.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node.
   * @param string $field
   *   The field.
   * @param string $view_field
   *   The view field.
   *
   * @return array
   *   get view Field.
   */
  public function viewField(NodeInterface $node, string $field, string $view_field) {
    if ($this->getContentView()) {
      return $this->viewBuilder->viewField(
        $node->get($field),
        ['label' => 'hidden'] + ($this->getContentView()->getHandler('default', 'field', $view_field) ?? [])
      );
    }
    return $this->viewBuilder->viewField($node->get($field), ['label' => 'hidden']);
  }

  /**
   * Return content view.
   *
   * @return \Drupal\views\ViewExecutable|false
   *   get Content View.
   */
  public function getContentView() {
    if ($this->contentView instanceof ViewExecutable || $this->contentView === FALSE) {
      return $this->contentView;
    }
    $content_view = $this->entityTypeManager->getStorage('view')->load('content');
    if (!$content_view) {
      return $this->contentView = FALSE;
    }
    return $this->contentView = $content_view->getExecutable();
  }

  /**
   * Get available actions.
   *
   * @return array
   *   get Actions.
   */
  public function getActions() {
    return $this->actions;
  }

  /**
   * Clear route definitions if menu settings has changed.
   */
  public static function clearRouteDefinitionsCache(&$form, FormStateInterface $form_state) {
    $menu = $form_state->getFormObject()->getEntity();
    if ($menu->isNew() || !isset($menu->original) ||
      ($menu->get('content_hub_tree') != $menu->original->get('content_hub_tree'))) {
      \Drupal::service('router.builder')->rebuild();
    }
  }

}
